import requests
from bs4 import BeautifulSoup
import os
import sys
from dotenv import load_dotenv
import pika
from initr import addref
import sentry_sdk
sentry_sdk.init(
    "https://f38af35425b9426b9bf053e23acc0f88@o1092289.ingest.sentry.io/6144373",

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0
)

urlcntr='https://en.wikipedia.org/wiki/List_of_countries_by_population_(United_Nations)'

load_dotenv()
if len(sys.argv)>1 and sys.argv[1]=='test':
# if os.getenv('TESTAP'):
    parameters = pika.URLParameters(f"amqp://{os.getenv('TESTRMUSER')}:{os.getenv('TESTRMPASS')}@{os.getenv('TESTRMHOST')}:{'5672'}")
else:
    parameters = pika.URLParameters(f"amqp://{os.getenv('RMUSER')}:{os.getenv('RMPASS')}@{os.getenv('RMHOST')}:{'5672'}") 
parameters.blocked_connection_timeout=600
parameters.heartbeat=600    
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='app4')
connection1 = pika.BlockingConnection(parameters)
channel1 = connection1.channel()
channel1.queue_declare(queue='app5')

def init_ch():
  global channel1
  global connection1
  global parameters
  if channel1.is_closed or connection1.is_closed:
    connection1 = pika.BlockingConnection(parameters)  
    channel1 = connection.channel()
    channel1.queue_declare(queue='app5')
  return channel1

def read_cntr(url):
    '''return list of countries from url'''
    res=requests.get(url)
    if res.status_code == 200:
        data= BeautifulSoup(res.text, "html.parser").find_all("table",{"class": "wikitable"})[0].find_all('td')
        arr={}
        for x in data[0::6]:
            for y in x.find_all('a'):
                arr[y.text.upper()]=y.get('href')
        return arr        
    return None

cntr_arr=read_cntr(urlcntr)

def cb(ch, method, properties, body):
   global cntr_arr  
   try: 
     buf=str(body,'utf-8').split(',')
     code=buf[0].upper() 
     if code:
       try:
         ref=cntr_arr[code]
       except:
         sentry_sdk.capture_message(f'country not found {code}')  
       if ref != None:
         refbody=f'{buf[0]},{buf[1]},{ref}'
         addref(code,ref)
         chnl=init_ch()
         chnl.basic_publish(exchange="",routing_key="app5", body=refbody)
       else:sentry_sdk.capture_message(f'country not found {code}')  
   except:return None   
      

channel.basic_consume(queue='app4', on_message_callback=cb, auto_ack=True)
while True:
    try:
      channel.start_consuming()
    except: None  



import os
from dotenv import load_dotenv
from fpdf import FPDF
from initpost import post_mail
from initdb import dbeng
import pika
import sentry_sdk
sentry_sdk.init(
    "https://f38af35425b9426b9bf053e23acc0f88@o1092289.ingest.sentry.io/6144373",

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0
)

load_dotenv()
parameters = pika.URLParameters(f"amqp://{os.getenv('RMUSER')}:{os.getenv('RMPASS')}@{os.getenv('RMHOST')}:{'5672'}")
parameters.blocked_connection_timeout=600
parameters.heartbeat=600
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='app7')

def sendmail():
    arr=dbeng.getjrn()
    fname='data.pdf'
    if arr !=None and len(arr)>0:
        pdf = FPDF()
        pdf.set_font("Arial", size=10)
        pdf.add_page()
        for col in arr:
            for x in col:pdf.cell(60,10,txt=str(x), border=1)
            pdf.ln()
        pdf.output(fname)
        with open(fname,'rb') as f:
            buf=f.read()
        arr=dbeng.getusers()
        sentry_sdk.capture_message(f'send file  {fname}')
        if len(arr)>0:
            for x in arr:
                post_mail('hw23 jornal',x,'For home work 23',fname,buf)
        else: sentry_sdk.capture_message(f'user list not found')        
    else : sentry_sdk.capture_message(f'array not found')           

def cb(ch, method, properties, body):
    try:
        # if body==b'send': 
            sendmail()
    except:None

channel.basic_consume(queue='app7', on_message_callback=cb, auto_ack=True)
while True:
    try:
      channel.start_consuming()
    except: None  
import os
from dotenv import load_dotenv
import smtplib
from datetime import datetime
from email.message import EmailMessage
load_dotenv()

def post_mail(subj:str,adrto:str,mess:str,fname:str,filebuf):
    '''Post message email with attachment in file'''
    EMAIL_PASS=os.getenv('PASS')
    EMAIL_US=os.getenv('MAILUSER')
    msg=EmailMessage()
    msg['Subject']=subj
    msg['From']=EMAIL_US
    msg['To']=adrto
    dt=datetime.now().strftime("%A, %d. %B %Y %I:%M%p")
    msg.set_content(f'From: app_4 <{EMAIL_US}>\nSent:{dt}\n\n\n'+mess)
    if filebuf:
        msg.add_attachment(filebuf,maintype='application',subtype='csv',filename=fname)
          
    with smtplib.SMTP_SSL('smtp.mail.ru',465) as smtp:
        smtp.login(EMAIL_US,EMAIL_PASS)
        smtp.send_message(msg)
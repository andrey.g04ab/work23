import requests
from bs4 import BeautifulSoup
from initpost import post_mail
import os
from dotenv import load_dotenv
import pika
import sentry_sdk
sentry_sdk.init(
    "https://f38af35425b9426b9bf053e23acc0f88@o1092289.ingest.sentry.io/6144373",

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0
)
wikiurl='https://en.wikipedia.org'

load_dotenv()
parameters = pika.URLParameters(f"amqp://{os.getenv('RMUSER')}:{os.getenv('RMPASS')}@{os.getenv('RMHOST')}:{'5672'}")
parameters.blocked_connection_timeout=600
parameters.heartbeat=600
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='app5')

def read_ref(url):
    try:
      if url:
           res=requests.get(url, headers={'User-Agent':'Mozilla'})
           if res.status_code == 200:
                return BeautifulSoup(res.text, "html.parser").find('audio').find('source').get('src')
      return None             
    except:
      return None   

def read_himn(url):
    try:
      if url!=None:
           res=requests.get(url, headers={'User-Agent':'Mozilla'})
           if res.status_code == 200:
             return res.content
      return None          
    except:
        return None  

def cb(ch, method, properties, body):
    res=str(body,'utf-8').split(',')
    ref=read_ref(wikiurl+res[2]).strip()
    if ref !=None:
      buf=read_himn('https:'+ref)
      fname=res[0]+ref[-4::]
      post_mail(f'Himn {res[0]}',res[1],'For home work 23',fname,buf)
    else:
      sentry_sdk.capture_message(f'country not found {res[0]}')


channel.basic_consume(queue='app5', on_message_callback=cb, auto_ack=True)
while True:
    try:
      channel.start_consuming()
    except: None          
import os
from dotenv import load_dotenv
import pytest
import pika
from app1.app import app
from app1.initdb import dbeng

load_dotenv()
os.environ['TESTAP']='1'

@pytest.fixture
def client():
    yield app.test_client()

def test_req(client):
    res=client.get('/')
    assert res.status_code==200
    assert b'hwork_23' in res.data

def test_user(client):
    dbeng.delusers()
    data={'mail':'testm@test.com'}
    res=client.post('/user',json=data)
    assert res.status_code==200
    res=client.post('/get',json=data)
    assert res.data==b"['testm@test.com']"    

def test_data(client):
    parameters = pika.URLParameters(f"amqp://{os.getenv('TESTRMUSER')}:{os.getenv('TESTRMPASS')}@{os.getenv('TESTRMHOST')}:{'5672'}")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='app5')
    channel1 = connection.channel()
    channel1.queue_declare(queue='app4')
    channel1.basic_publish(exchange="",routing_key="app4", body='france,testm@test.com') 
    for mf, prop, body in channel.consume('app5',inactivity_timeout=6):
      if body:  
        res=str(body,'utf-8').split(',')
        print(res)
      else:res=['0','0','0']  
      assert res[0].upper()=='FRANCE'
      assert res[1]=='testm@test.com'
      assert res[2].upper()=='/WIKI/FRANCE'
      break
    channel.close()
    connection.close()
    
if __name__=="__main__":
    pytest.main() 
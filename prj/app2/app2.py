from initdb import dbeng
import os
from dotenv import load_dotenv
import pika
import sentry_sdk
sentry_sdk.init(
    "https://f38af35425b9426b9bf053e23acc0f88@o1092289.ingest.sentry.io/6144373",

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0
)

def cb(ch, method, properties, body):
   try: 
        res=str(body,'utf-8').split(',')
        dbeng.addjrn(res[0],res[1]) 
   except Exception as exc:
         sentry_sdk.capture_exception(exc)            

load_dotenv()
def connect():
    parameters = pika.URLParameters(f"amqp://{os.getenv('RMUSER')}:{os.getenv('RMPASS')}@{os.getenv('RMHOST')}:{'5672'}")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange="message", exchange_type="fanout")
    result = channel.queue_declare(queue="")
    qname = result.method.queue
    channel.queue_bind(exchange="message", queue=qname)
    channel.basic_consume(queue=qname, on_message_callback=cb, auto_ack=True)
    return channel

channel=connect()
while True:
    try:
      channel.start_consuming()
    except: None  
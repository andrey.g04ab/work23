from sqlalchemy import Column, Integer, String, DateTime 
from sqlalchemy.ext.declarative import declarative_base  
from sqlalchemy.orm import sessionmaker  
from sqlalchemy import create_engine  
from datetime import datetime
import os
from dotenv import load_dotenv

Base = declarative_base()  

class Users(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    mail = Column(String(100),nullable=False)
    def __init__(self,  mail:str) -> None:
        self.mail = mail[0:100]

class Jrnload(Base):
    __tablename__ = "jrnreq"
    id = Column(Integer, primary_key=True)
    code = Column(String(100),nullable=False)
    mail = Column(String(100),nullable=False)
    dt = Column(DateTime)
    
    def __init__(self, code:str, mail:str) -> None:
        self.code = code.strip().upper()[0:10]
        self.mail = mail[0:100]
        self.dt=datetime.now()

class Dbeng:
    def __init__(self, session) -> None:
        self.session = session
    
    def getusers(self):
        arr=[]
        for rec in self.session.query(Users).all():
            arr.append(rec.mail)
        return arr 

    def delusers(self):
        for rec in self.session.query(Users).all():
            self.session.delete(rec)
        self.session.commit()
    
    def deljrn(self):
        for rec in self.session.query(Jrnload).all():
            self.session.delete(rec)
        self.session.commit()    
         
    
    def adduser(self,mail:str):
        rec= self.session.query(Users).filter_by(mail=mail).first()
        if not rec:
            rec=Users(mail)                 
            self.session.add(rec)
            self.session.commit()
            return mail
        return None    
    
    def addjrn(self,code:str,mail:str): 
        rec=Jrnload(code,mail)                 
        self.session.add(rec)
        self.session.commit()
    
    def getjrn(self): 
        arr=[]
        for rec in self.session.query(Jrnload).all():
            arr.append([rec.dt,rec.code,rec.mail])
        return arr     

load_dotenv()
db = create_engine(f"postgresql://{os.getenv('TESTPOSTGRES_USER')}:{os.getenv('TESTPOSTGRES_PASSWORD')}@{os.getenv('TESTPOSTGRES_HOST')}:{os.getenv('TESTPOSTGRES_PORT')}/{os.getenv('POSTGRES_DB')}")
Base.metadata.bind = db
Base.metadata.create_all(db)        
dbeng=Dbeng(sessionmaker(bind=db)())
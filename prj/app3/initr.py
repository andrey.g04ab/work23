import os
from dotenv import load_dotenv
import redis
from celery import Celery 

load_dotenv()
cache = redis.Redis(host=os.getenv('RHOST'), port=os.getenv('RPORT'))
app = Celery('app6',backend="rpc://", broker=f"pyamqp://{os.getenv('RMUSER')}:{os.getenv('RMPASS')}@{os.getenv('RMHOST')}:{'5672'}")

@app.task
def getref(cntr):
    try:
      res=cache.get(cntr)
      return str(res,'utf-8')
    except:
        return None  
 

@app.task
def addref(cntr:str,ref:str):
    try:
         cache.mset({cntr:ref})
         return None
    except:
         return 'error'     


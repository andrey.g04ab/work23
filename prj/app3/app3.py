import os
from dotenv import load_dotenv
import pika
from initr import getref

load_dotenv()
if os.getenv('TESTAP'):
    parameters = pika.URLParameters(f"amqp://{os.getenv('TESTRMUSER')}:{os.getenv('TESTRMPASS')}@{os.getenv('TESTRMHOST')}:{'5672'}")
else:
    parameters = pika.URLParameters(f"amqp://{os.getenv('RMUSER')}:{os.getenv('RMPASS')}@{os.getenv('RMHOST')}:{'5672'}") 
parameters.blocked_connection_timeout=600
parameters.heartbeat=600    
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
connection1 = pika.BlockingConnection(parameters)
channel1 = connection1.channel()
channel1.queue_declare(queue='app4')
channel1.queue_declare(queue='app5')


def init_ch():
  global channel1
  global connection1
  global parameters
  if channel1.is_closed or connection1.is_closed:
    connection1 = pika.BlockingConnection(parameters)  
    channel1 = connection.channel()
    channel1.queue_declare(queue='app4')
    channel1.queue_declare(queue='app5')
  return channel1


def cb(ch, method, properties, body):
   try: 
     res=str(body,'utf-8').split(',')
     ref=getref.delay(res[0].upper()).get(timeout=1) 
     chnl=init_ch()
     if ref:
        chnl.basic_publish(exchange="",routing_key="app5", body=f'{res[0]},{res[1]},{ref}')
     else:   
        chnl=init_ch()
        chnl.basic_publish(exchange="",routing_key="app4", body=body)     
     return None   
   except: return None   
         

channel.exchange_declare(exchange="message", exchange_type="fanout")
result = channel.queue_declare(queue="")
qname = result.method.queue
channel.queue_bind(exchange="message", queue=qname)
channel.basic_consume(queue=qname, on_message_callback=cb, auto_ack=True)
while True:
    try:
      channel.start_consuming()
    except: None  

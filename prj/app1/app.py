from flask import Flask,jsonify, request
import os
from dotenv import load_dotenv
import pika
from initdb import dbeng
import sentry_sdk
sentry_sdk.init(
    "https://f38af35425b9426b9bf053e23acc0f88@o1092289.ingest.sentry.io/6144373",

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0
)

load_dotenv()

if os.getenv('TESTAP'):
    parameters = pika.URLParameters(f"amqp://{os.getenv('TESTRMUSER')}:{os.getenv('TESTRMPASS')}@{os.getenv('TESTRMHOST')}:{'5672'}")
else:
    parameters = pika.URLParameters(f"amqp://{os.getenv('RMUSER')}:{os.getenv('RMPASS')}@{os.getenv('RMHOST')}:{'5672'}")    
parameters.blocked_connection_timeout=600
parameters.heartbeat=600
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.exchange_declare(exchange="message", exchange_type="fanout")
channel1 = connection.channel()
channel1.queue_declare(queue='app7')

def init_ch():
  global channel
  global connection
  global parameters
  if channel.is_closed or connection.is_closed:
    connection = pika.BlockingConnection(parameters)  
    channel = connection.channel()
    channel.exchange_declare(exchange="message", exchange_type="fanout")
  return channel

def init_ch1():
    global channel1
    global connection
    global parameters
    if channel1.is_closed or connection.is_closed:
       connection = pika.BlockingConnection(parameters)
       channel1 = connection.channel()
       channel1.queue_declare(queue='app7')
    return channel1    

app = Flask(__name__)

@app.route("/",methods=["GET"])
def get_app():
    return 'hwork_23'

@app.route("/",methods=["POST"])
def get_cntr():
    try:
        ch1=init_ch()
        ch1.basic_publish(exchange="message", routing_key='', body=f'{request.json["code"]},{request.json["mail"]}')
        return 'processed...'
    except:
        return 'error'    

@app.route("/<param>",methods=["POST"])
def get_data(param):
    try:
        if param:
            if param.upper()=='GET':
                return str(dbeng.getusers())
            if param.upper()=='DELETE':
                dbeng.delusers()    
                return 'deleted'
            if param.upper()=='DELJRN':
                dbeng.deljrn()    
                return 'deleted jrn'    
            elif param.upper()=='USER':
                res=dbeng.adduser(request.json['mail'])
                if res==None:
                    return 'mail exist'
                else:
                    return request.json['mail']    
            elif param.upper()=='SEND':
               ch2=init_ch1()
               ch2.basic_publish(exchange="",routing_key="app7", body='send')
               return 'send mail'     
    except Exception as exc:
         sentry_sdk.capture_exception(exc)  
         return 'error'
    return 'hwork_23 service'

if __name__ == "__main__":
    app.run(host="0.0.0.0",debug=False,port=5000) 

